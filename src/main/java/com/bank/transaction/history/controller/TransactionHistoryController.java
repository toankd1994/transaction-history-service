package com.bank.transaction.history.controller;

import com.bank.transaction.history.Transaction;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api")
public class TransactionHistoryController {

    @Autowired
    private TransactionService transactionService;

    @GetMapping(value = "/v1/auth-test")
    @Operation(summary = "My endpoint", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<?> greeting(@RequestHeader HttpHeaders headers) {
        String token = headers.getFirst(HttpHeaders.AUTHORIZATION);
        log.info("Token = " + token);
        return ResponseEntity.ok("this is a secure api");
    }

    @Operation(summary = "Add transaction", security = @SecurityRequirement(name = "bearerAuth"))
    @PostMapping(value = "/v1/transaction/add", produces = "application/json")
    public ResponseEntity<?> addTxn(@Valid @RequestBody Transaction txn) {
        transactionService.add(txn);
        return ResponseEntity.ok("OK");
    }

    @GetMapping(value = "/v1/txn")
    @Operation(summary = "My endpoint", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<?> getTxn() {
        return ResponseEntity.ok(transactionService.getTxnList());
    }
}
